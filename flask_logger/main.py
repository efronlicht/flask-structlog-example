import urllib.parse
import uuid
from typing import Any

import flask
import structlog
import re

_possible_email: re.Pattern[str] = re.compile(r"^.+@.+\..+$")
_censored_keys: set[str] = {"Authorization", "Auth", "auth", "authorization"}
_omitted_keys: set[str] = {
    "Accept-Encoding",
    "Accept",
    "Sentry-Trace",
    "X-Datadog-Parent-Id",
    "X-Datadog-Sampling-Priority",
    "X-Datadog-Trace-Id",
    "X-Forwarded-For",
    "X-Forwarded-Port",
    "X-Forwarded-Proto",
}


app: flask.Flask = flask.Flask(__name__, instance_relative_config=True)


def just_path(r: flask.Request) -> str:
    """the path without the netloc"""
    parsed_url = urllib.parse.urlparse(r.base_url)
    return parsed_url.path


@app.before_request
def add_request_log_to_threadlocal_context():
    """add a prepopulated logger w/ request info to flask's threadlocal context under the 'log' key."""
    headers = dict()
    r = flask.request
    for k, v in r.headers.items():
        if k in _omitted_keys:
            continue
        if k in _censored_keys or (_possible_email.match(v) is not None):
            headers[k] = "<omitted>"
            continue
        headers[k] = v
    path = just_path(r)
    flask.g.logger = structlog.get_logger(
        request={
            "id": str(uuid.uuid4()),
            "header": headers,
            "url": {
                "raw": r.full_path,
                "path": path,
                "raw_query": r.args.to_dict(),
                "query": r.args.to_dict(),
                "raw_query": r.query_string,
            },
            "method": r.method,
        }
    )
    flask.g.logger.debug(f"{r.method} {path}: start")
    return


@app.after_request
def log_response(resp: flask.Response) -> flask.Response:
    logger: structlog.PrintLogger = flask.g.logger.bind(status_code=resp.status_code)
    if resp.status_code >= 300:
        logger.error(
            f"{flask.request.method} {just_path(flask.request)} end: error: {resp.status}"
        )
    else:
        logger.debug(
            f"{flask.request.method} {just_path(flask.request)} end: success: {resp.status}"
        )
    return resp


@app.route("/")
def hello_world() -> str:
    return "Hello, world!"


def main():
    app.run(debug=True)


if __name__ == "__main__":
    main()
